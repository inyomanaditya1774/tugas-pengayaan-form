<?php
//memasukkan file config.php
include('config.php');
?>
   
   <div class="container" style="margin-top:20px">
		<center><font size="6">Jadwal Kelas</font></center>
		<hr>
		<a href="index.php?page=tambah_jdwl"><button class="btn btn-dark right">Tambah Data</button></a>
		<div class="table-responsive">
		<table class="table table-striped jambo_table bulk_action">
			<thead>
				<tr>
					<th>id_jadwal</th>
					<th>id_dosen</th>
					<th>id_kelas</th>
					<th>jadwal</th>
					<th>mata_kuliah</th>
				</tr>
			</thead>
			<tbody>
				<?php
				//query ke database SELECT tabel jadwal kelas urut berdasarkan id yang paling besar
				$sql = mysqli_query($koneksi, "SELECT * FROM jadwal ORDER BY id_jadwal DESC") or die(mysqli_error($koneksi));
				//jika query diatas menghasilkan nilai > 0 maka menjalankan script di bawah if...
				if(mysqli_num_rows($sql) > 0){
					//membuat variabel $no untuk menyimpan nomor urut
					//$no = 1;
					//melakukan perulangan while dengan dari dari query $sql
					while($data = mysqli_fetch_assoc($sql)){
						//menampilkan data perulangan
						echo '
						<tr>					
							<td>'.$data['id_jadwal'].'</td>
							<td>'.$data['id_dosen'].'</td>
							<td>'.$data['id_kelas'].'</td>
							<td>'.$data['jadwal'].'</td>
							<td>'.$data['mata_kuliah'].'</td>
							<td>
								<a href="index.php?page=edit_jdwl&id_jadwal='.$data['id_jadwal'].'" class="btn btn-secondary btn-sm">Edit</a>
								<a href="delete.php?id_jadwal='.$data['id_jadwal'].'" class="btn btn-danger btn-sm" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
							</td>
						</tr>
						';
						$no++;
					}
				//jika query menghasilkan nilai 0
				}else{
					echo '
					<tr>
						<td colspan="8">Tidak ada data.</td>
					</tr>
					';
				}
				?>
			<tbody>
		</table>
	</div>
</div>